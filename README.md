## Note transformation test task.

Input JSON example:
`[
    {
        "octaveNumber": 2,
        "noteNumber": 1
    },
    {
        "octaveNumber": 2,
        "noteNumber": 6
    }
]`

Program arguments example (absolute path, semitone number):
`java -jar test.jar C:/notes.json -3`

P.s. There is a unit test which tests transformation function. Input and output taken
from the task.