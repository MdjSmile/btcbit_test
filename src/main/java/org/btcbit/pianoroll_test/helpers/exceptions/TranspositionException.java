package org.btcbit.pianoroll_test.helpers.exceptions;

public class TranspositionException extends RuntimeException {

    public TranspositionException(String message) {
        super(message);
    }
}