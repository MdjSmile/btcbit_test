package org.btcbit.pianoroll_test.helpers;

import org.btcbit.pianoroll_test.dtos.Note;
import org.btcbit.pianoroll_test.helpers.exceptions.TranspositionException;

import java.util.List;
import java.util.stream.Collectors;

public final class TranspositionHelper {

    public final static int MAX_OCTAVES = 5;
    public final static int MIN_OCTAVES = -3;
    public final static int MAX_SEMITONE = 12;
    public final static int MAX_NOTE_OF_FIFTH_OCTAVE = 1;
    public final static int MIN_NOTE_OF_THIRD_OCTAVE = 10;

    public static List<Note> transpose(List<Note> notes, int semitones) {
        return notes.stream()
                .map(note -> transposeNote(note, semitones))
                .collect(Collectors.toList());
    }

    private static Note transposeNote(Note note, int semitone) throws TranspositionException {
        var newNoteNum = note.getNoteNumber() + semitone % MAX_SEMITONE;
        var newNote = new Note();
        if (newNoteNum <= 0) {
            var newOctave = note.getOctaveNumber() - 1 + semitone / MAX_SEMITONE;
            var fixedNote = MAX_SEMITONE + newNoteNum;
            if (newOctave < MIN_OCTAVES || newOctave == MIN_OCTAVES && fixedNote < MIN_NOTE_OF_THIRD_OCTAVE) {
                throw new TranspositionException(String.format("Too low note: [%d,%d]. Should be between [%d,%d] and [%d,%d]",
                        newOctave, fixedNote, MIN_OCTAVES, MIN_NOTE_OF_THIRD_OCTAVE, MAX_OCTAVES, MAX_NOTE_OF_FIFTH_OCTAVE));
            }

            newNote.setNoteNumber(fixedNote);
            newNote.setOctaveNumber(newOctave);

            return newNote;
        } else if (newNoteNum > MAX_SEMITONE) {
            var newOctave = note.getOctaveNumber() + 1 + semitone / MAX_SEMITONE;
            var fixedNote = newNoteNum - MAX_SEMITONE;
            if (newOctave > MAX_OCTAVES || newOctave == MAX_OCTAVES && fixedNote > MAX_NOTE_OF_FIFTH_OCTAVE) {
                throw new TranspositionException(String.format("Too high note: [%d,%d], Should be between [%d,%d] and [%d,%d]",
                        newOctave, fixedNote, MIN_OCTAVES, MIN_NOTE_OF_THIRD_OCTAVE, MAX_OCTAVES, MAX_NOTE_OF_FIFTH_OCTAVE));
            }

            newNote.setOctaveNumber(newOctave);
            newNote.setNoteNumber(fixedNote);

            return newNote;
        }

        newNote.setOctaveNumber(note.getOctaveNumber() + semitone / MAX_SEMITONE);
        newNote.setNoteNumber(newNoteNum);
        return newNote;
    }

    private TranspositionHelper() {
    }
}