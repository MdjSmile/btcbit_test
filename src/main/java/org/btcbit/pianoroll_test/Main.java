package org.btcbit.pianoroll_test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.btcbit.pianoroll_test.dtos.Note;
import org.btcbit.pianoroll_test.helpers.TranspositionHelper;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Notes transposition.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        var path = Path.of(ClassLoader.getSystemResource("notes.json").toURI());
        var json = args.length == 2 ? Files.readString(Path.of(args[0])) : Files.readString(path);
        var notes = new ObjectMapper().readValue(json, new TypeReference<List<Note>>() {
        });

        System.out.println(String.format("Input notes (%s): %s", path.toAbsolutePath(), notes));
        var semitones = args.length == 2 ? Integer.parseInt(args[1]) : -3;
        System.out.println(String.format("Transposed notes to %d: %s", semitones, TranspositionHelper.transpose(notes, semitones)));
    }
}