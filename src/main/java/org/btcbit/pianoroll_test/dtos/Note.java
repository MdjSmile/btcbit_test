package org.btcbit.pianoroll_test.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Note {

    @JsonProperty
    private int octaveNumber;

    @JsonProperty
    private int noteNumber;

    @Override
    public String toString() {
        return "[" + octaveNumber + "," + noteNumber + "]";
    }
}