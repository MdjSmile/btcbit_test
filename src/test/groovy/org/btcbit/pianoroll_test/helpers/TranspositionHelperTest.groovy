package org.btcbit.pianoroll_test.helpers

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.btcbit.pianoroll_test.dtos.Note
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors
import java.util.stream.Stream

class TranspositionHelperTest extends Specification {

    def "transpose test"() {
        given:
        def notesPath = Path.of(ClassLoader.getSystemResource("notes.json").toURI())
        def json = Files.readString(notesPath)
        def notes = new ObjectMapper().readValue(json, new TypeReference<List<Note>>() {})
        def checkListPath = Path.of(ClassLoader.getSystemResource("check_transposition.txt").toURI())
        def checkList = Stream.of(Files.readString(checkListPath).split("],"))
                .map { it -> it.replace("[", "").replace("]", "").split(",") }
                .map { it -> List.of(Integer.parseInt(it[0]), Integer.parseInt(it[1])) }
                .collect(Collectors.toList())
        def result = TranspositionHelper.transpose(notes, -3)

        expect:
        result != null
        for (def i = 0; i < result.size(); i++) {
            def note = result.get(i)
            def checkNote = checkList.get(i)

            assert note.noteNumber == checkNote.get(1)
            assert note.octaveNumber == checkNote.get(0)
        }
    }
}